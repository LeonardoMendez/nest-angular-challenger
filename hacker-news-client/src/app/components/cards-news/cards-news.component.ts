import { Component, Input, OnInit } from '@angular/core';
import { CardNewsModels } from './model';
import { faTrash, IconDefinition } from '@fortawesome/free-solid-svg-icons';
import { NewsService } from '../../services/news.service';

@Component({
  selector: 'app-cards-news',
  templateUrl: './cards-news.component.html',
  styleUrls: ['./cards-news.component.css'],
})
export class CardsNewsComponent implements OnInit {
  @Input() cardNew: CardNewsModels;
  fatrash: IconDefinition;
  constructor(private newsService: NewsService ) {
    this.fatrash = faTrash;
  }

  ngOnInit() {}

  delete() {
    console.log("delete", this.cardNew._id);
    this.newsService
      .deleteNew(this.cardNew._id)
      .toPromise()
      .then((res) => window.location.reload())
      .catch((error) => console.log(error));
  }

  goToLink() {
    const urlDest =
      this.cardNew.story_url != null
        ? this.cardNew.story_url
        : this.cardNew.url;
    window.open(urlDest);
  }
}
