import { ObjectId } from 'bson';

export class NewsDto {
  _id: ObjectId;
  created_at: Date;
  story_title: string;
  title: string;
  story_url: string;
  url: string;
  author: string;
}
