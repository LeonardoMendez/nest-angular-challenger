import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'dateFormat',
})
export class DateFormatPipe implements PipeTransform {
  transform(value: Date, ...args: any[]): any {
    const aMomment = moment(value);
    const today = moment(Date.now());

    if (aMomment.isSame(today.startOf('day'), 'd')) {
      return aMomment.format('hh:mm a');
    }

    if (
      aMomment.isSame(today.subtract(1, 'days').startOf('day'), 'd')
    ) {
      return 'yesterday';
    }

    return aMomment.format('"MMM DD"');
  }
}
