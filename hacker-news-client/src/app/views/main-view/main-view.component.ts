import { AfterViewInit, Component, OnInit } from '@angular/core';
import { NewsDto } from '../../models/news.dto';
import { NewsService } from '../../services/news.service';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css'],
})
export class MainViewComponent implements OnInit {
  private cardNews: NewsDto[] = [];

  constructor(private newsService: NewsService) {
    this.loadPage();
  }

  ngOnInit() {}

  loadPage() {
    this.newsService
      .getIsDeleteFalse()
      .pipe(
        map((aNewsDto: NewsDto[]) =>
          aNewsDto.filter((elem: NewsDto) => elem.title || elem.story_title),
        ),
      )
      .subscribe((resCardNews) => (this.cardNews = resCardNews));
  }

  cargar() {
    console.log('load base');
    this.newsService
      .loadDb()
      .toPromise()
      .then((res) => {
        console.log(res);
      })
      .catch((error) => console.log(error));
  }


}
