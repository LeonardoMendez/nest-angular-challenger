import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NewsDto } from '../models/news.dto';
import { ObjectId } from 'bson';

@Injectable({
  providedIn: 'root',
})
export class NewsService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<NewsDto[]> {
    return this.http.get<NewsDto[]>('http://localhost:3000/news/allbd');
  }

  getIsDeleteFalse(): Observable<NewsDto[]> {
    return this.http.get<NewsDto[]>('http://localhost:3000/news/notdeleted');
  }

  loadDb(): Observable<NewsDto[]> {
    return this.http.post<NewsDto[]>('http://localhost:3000/news/load ', {});
  }

  deleteNew(_id: ObjectId) {
    return this.http.delete('http://localhost:3000/news/' + _id);
  }
}
