import { Document } from 'mongoose';

export interface NewsInterface extends Document {
  readonly story_id: number;
  readonly created_at: Date;
  readonly story_title: string;
  readonly title: string;
  readonly author: string;
  readonly story_url: string;
  readonly url: string;
  readonly isDelete: boolean;
}
