import { HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { HistDto } from './dto/hist.dto';
import { NewsInterface } from './interfaces/news.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel('News') private readonly newsModels: Model<NewsInterface>,
    private httpService: HttpService,
  ) {}

  returnHits(): Promise<AxiosResponse<HistDto>> {
    return this.httpService
      .get<HistDto>('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
  }

  async loadDataBase(): Promise<NewsInterface[]> {
    const todayInsert = await this.findInsertYesterdayToToday();
    return await this.httpService
      .get<HistDto>('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(
        map((response) =>
          response.data.hits.filter((aNews: NewsInterface) => this.contain(aNews,todayInsert)),
        ),
        map((aNews) => this.newsModels.insertMany(aNews)),
      )
      .toPromise();
  }

  async deleteLogical(id): Promise<NewsInterface> {
    return await this.newsModels.findByIdAndUpdate(id, { isDelete: true });
  }

  async findAll(): Promise<NewsInterface[]> {
    const news = await this.newsModels.find();
    return news;
  }

  async findAllNotDelete(): Promise<NewsInterface[]> {
    const query = {
      isDelete: false,
    };
    return await this.newsModels.find(query).sort({ created_at: -1 });
  }


  private findInsertYesterdayToToday() {
    const today = moment().startOf('day');
    const yesterday = moment().startOf('day').subtract(1, 'day');
    const query = {};
    query['created_at'] = {
      $gte: yesterday.toDate(),
      $lte: moment(today).endOf('day').toDate(),
    };
    return this.newsModels.find(query);
  }

  private contain(aNews: NewsInterface, todayInsert: NewsInterface[]) {
    const res = todayInsert.filter( (elem: NewsInterface) => elem.story_id === aNews.story_id );
    console.log("contiene", res.length);
    return res.length == 0;
  }
}
