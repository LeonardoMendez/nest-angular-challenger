import { Schema } from 'mongoose';

export const NewsSchema = new Schema({
  story_id: Number,
  story_title: String,
  title: String,
  story_url: String,
  url: String,
  author: String,
  created_at: Date,
  isDelete: {
    type: Boolean,
    default: false,
  },
});
