import { NewsInterface } from '../interfaces/news.interface';

export class HistDto {
  hits: NewsInterface[];
}
