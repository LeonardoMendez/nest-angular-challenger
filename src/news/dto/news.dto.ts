export class NewsDto {
  story_id: number;
  created_at: Date;
  story_title: string;
  title: string;
  story_url: string;
  url: string;
  author: string;
  isDelete: boolean;

}
