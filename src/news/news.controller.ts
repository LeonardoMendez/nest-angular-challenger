import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Res,
} from '@nestjs/common';
import { Response } from 'express';
import { NewsService } from './news.service';
import { NewsInterface } from './interfaces/news.interface';
import { ObjectId } from 'bson';

@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {}

  @Get('all')
  async findLast() {
    const response = await this.newsService.returnHits();
    return response.data.hits;
  }

  @Get('allbd')
  async findAll() {
    return await this.newsService.findAll();
  }

  @Get('notdeleted')
  async findAllIsDeleteFalse() {
    return await this.newsService.findAllNotDelete();
  }

  @Delete(':id')
  async deleteNew(@Res() res: Response, @Param('id') id: ObjectId) {
    console.log('entre al controler borrar publicacion ' + id);
    await this.newsService.deleteLogical(id);
    return res.status(HttpStatus.OK).json({ ok: 'success' });
  }

  @Post('load')
  loadBase(@Res() res: Response) {
    console.log('entre al controler caragar la base');
    this.newsService.loadDataBase().then((news: NewsInterface[]) => {
      res.status(HttpStatus.OK).json(news);
    });
  }
}
